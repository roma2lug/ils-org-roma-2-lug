---
layout: post
title: Cyber Resilience Act
image: /assets/posts/images/europarlamento.jpg
image_copy: <a rel="nofollow" href="https://commons.wikimedia.org/wiki/File:European_Parliament_Strasbourg_Hemicycle_-_Diliff.jpg">Diliff, CC BY-SA 3.0, via Wikimedia Commons</a>
---

La Commissione Europea è al lavoro su [una direttiva](https://digital-strategy.ec.europa.eu/en/library/cyber-resilience-act) denominata **Cyber Resilience Act**, o semplicemente **CRA**, che prevede regole potenzialmente dannose per lo sviluppo e la diffusione del software libero e open source in Europa.

<!--more-->

Nelle scorse settimane e mesi molti dei più rilevanti soggetti operanti per la promozione e la tutela del software open source hanno [espresso](https://blog.opensource.org/the-ultimate-list-of-reactions-to-the-cyber-resilience-act/) [preoccupazione](https://newsroom.eclipse.org/news/announcements/open-letter-european-commission-cyber-resilience-act) nei confronti delle implicazioni di questa proposta di legge, ed hanno sollecitato [correzioni ed emendamenti](https://ec.europa.eu/info/law/better-regulation/have-your-say/initiatives/13410-Cyber-resilience-act-new-cybersecurity-rules-for-digital-products-and-ancillary-services/F3376663_en) che possano in qualche modo far convivere le buone intenzioni della norma (dedicata all'incremento della sicurezza del software e degli apparati elettronici quotidianamente utilizzati da milioni di cittadini europei) con le dinamiche (spesso informali, e proprio per questo difficilmente regolamentabili) proprie del modello di sviluppo open.

Diversi sono i punti contesi, accomunati da un unico principio: gli autori di un progetto open source (e chi vi contribuisce, e chi lo distribuisce) non possono essere considerati diretti responsabili per danni arrecati a terzi, laddove tale progetto è per definizione liberamente utilizzabile da chiunque e per qualsiasi scopo.

Il patto storico del modello open è sempre stato molto semplice: "questo è il software, è gratuito, non c'è nessuna garanzia". E proprio questo patto - sancito anche dal punto di vista legale in pressoché ogni licenza [approvata da Open Source Initiative](https://opensource.org/licenses/) - garantisce non solo la libertà di utilizzo, analisi e modifica del codice, ma è anche alla base di qualsiasi modello di business incentrato nel dare all'utilizzatore finale quelle stesse garanzie non fornite alla fonte (a fronte di un lecito e doveroso compenso economico). Minare tale patto con una forzatura normativa calata dall'alto implica infrangere le più essenziali dinamiche che distinguono un fenomeno - culturale oltre che tecnico - che da almeno vent'anni è vettore di progresso tecnologico e crescita economica, in Europa e nel mondo.

Per tutti questi motivi, appurato che sono comunque già in corso consultazioni e pressioni affinché la bozza della direttiva venga corretta prima del voto presso il Parlamento Europeo, Italian Linux Society vi invita ad inviare una mail di sensibilizzazione agli europarlamentari italiani. In particolare ai membri delle commissioni ["International Trade"](https://www.europarl.europa.eu/committees/en/inta/home/highlights) e ["Industry, Research and Energy"](https://www.europarl.europa.eu/committees/en/itre/home/highlights), che in prima persona sono responsabili della revisione degli emendamenti proposti.

Qui di seguito un modello di lettera (elaborato a partire da quello proposto da Moreno Razzoli, in arte MorroLinux, che grazie a [questo video](https://www.youtube.com/watch?v=nYhRo74yqJg) ha portato il tema all'attenzione del pubblico italiano) e, sotto, i riferimenti (email ma anche social, per chi volesse estendere il raggio di azione della propria attività di informazione) delle persone interessate.

<hr>

<blockquote class="blockquote">
<p>
Onorevole [Cognome del MEP],
</p>

<p>
Nei prossimi mesi il Parlamento Europeo sarà chiamato per l'approvazione della direttiva denominata "Cyber Resilience Act" (CRA).
</p>

<p>
Questo provvedimento rischia di danneggiare gravemente la comunità open source e, conseguentemente, le migliaia di piccole, medie e grandi realtà professionali e industriali che grazie al patrimonio di componenti open source (liberamente e spesso gratuitamente accessibili) realizzano e distribuiscono soluzioni innovative in Europa.
</p>

<p>
L'intento della direttiva è certamente condivisibile, ma nella sua attuale formulazione risulta problematico in più punti:
</p>
<ul>
<li>le premesse della direttiva introducono delle generiche eccezioni per il software open source, identificandone e riconoscendone fin da subito il valore e l'impatto nei confronti del sistema produttivo (europeo e non), ma sono essenzialmente invalidate dalla scarsa definizione di "fini commerciali"; il termine "commerciale", storicamente ritenuto ambiguo nel contesto legislativo, non aiuta a delimitare un reale perimetro di competenza e responsabilità</li>
<li>l'articolo 4, comma 3, delinea restrizioni per la distribuzione di "software incompleto" basate su una concezione anacronistica dello sviluppo del software e antitetiche al moderno modello di sviluppo tecnologico, fondato proprio sulla continua iterazione ed evoluzione dei singoli elementi che ne fanno parte</li>
<li>l'articolo 14 enumera una serie di responsabilità in carico alla figura del "distributore" del software, ma fallisce nell'identificare questo ruolo. Altre norme europee esistenti (in particolare: il Digital Services Act e la Copyright Directive) hanno espressamente introdotto delle eccezioni nei confronti delle piattaforme di hosting software, comunemente usate per attingere ad una enorme quantità di risorse software condivise che accelerano la creazione e la manutenzione di prodotti e soluzioni tecnologiche; senza tali eccezioni, il mercato europeo rischia di essere isolato da tali importanti risorse e privato di un importante vantaggio competitivo rispetto al resto del mondo</li>
<li>l'articolo 16 sancisce che "chi applica modifiche sostanziali" ad un progetto è considerato al pari dell'autore in termini di responsabilità: questo appare essere un forte disincentivo alla collaborazione intrinseca innescata dal modello di sviluppo open source, che permette ad ogni operatore commerciale - piccolo o grande che sia - di giovare dei perfezionamenti introdotti da tutti gli altri e di mantenere un costante livello di aggiornamento tecnologico e di competitività</li>
</ul>

<p>
Tra gli innumerevoli enti che hanno espresso perplessità e hanno sollecitato interventi per correggere il testo della direttiva, cito qui:
</p>
<ul>
<li>Open Forum Europe, think tank della Commissione Europea<br>
https://ec.europa.eu/info/law/better-regulation/have-your-say/initiatives/13410-Cyber-resilience-act-new-cybersecurity-rules-for-digital-products-and-ancillary-services/F3376663_en</li>
<li>Asstel, associazione italiana della filiera delle telecomunicazioni<br>
https://ec.europa.eu/info/law/better-regulation/have-your-say/initiatives/13410-Cyber-resilience-act-new-cybersecurity-rules-for-digital-products-and-ancillary-services/F3376525_en</li>
</ul>

<p>
Con questo mio messaggio la invito a mia volta a non approvare la proposta nella sua attuale forma, e a supportare e sostenere emendamenti correttivi ai sopra menzionati articoli.
</p>

<p>
Grazie per l'attenzione.
</p>
<p>
Cordiali saluti.
</p>
</blockquote>

<hr>

<div class="table-responsive">
    <table class="table">
        <thead>
        	<tr>
        		<td>Commissione</td>
        		<td>Nome</td>
        		<td>Cognome</td>
        		<td>Email</td>
        		<td>Twitter</td>
        		<td>Facebook</td>
        		<td>Instagram</td>
        	</tr>
        </thead>
        <tbody>
        	<tr>
        		<td>ITRE</td>
        		<td>Massimiliano</td>
        		<td>Salini</td>
        		<td>massimiliano.salini@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/MaxSalini">MaxSalini</a></td>
        		<td><a href="https://www.facebook.com/massimilianosalini.it">massimilianosalini.it</a></td>
        		<td><a href="https://www.instagram.com/massimilianosalini">massimilianosalini</a></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Elena</td>
        		<td>Lizzi</td>
        		<td>elena.lizzi@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/elenalizzi">elenalizzi</a></td>
        		<td><a href="https://www.facebook.com/scrivolizzi/">scrivolizzi</a></td>
        		<td><a href="https://www.instagram.com/elena_lizzi_/">elena_lizzi_</a></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Matteo</td>
        		<td>Gazzini</td>
        		<td>matteo.gazzini@europarl.europa.eu</td>
        		<td><br></td>
        		<td><br></td>
        		<td><br></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Gianna</td>
        		<td>Gancia</td>
        		<td>gianna.gancia@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/giannagancia">giannagancia</a></td>
        		<td><a href="https://www.facebook.com/giannagancia/">giannagancia</a></td>
        		<td><br></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Pietro</td>
        		<td>Fiocchi</td>
        		<td>pietro.fiocchi@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/FiocchiPietro">FiocchiPietro</a></td>
        		<td><a href="https://www.facebook.com/On.PietroFiocchi/">On.PietroFiocchi</a></td>
        		<td><a href="https://www.instagram.com/pietro_fiocchi">pietro_fiocchi</a></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Francesca</td>
        		<td>Donato</td>
        		<td>francesca.donato@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/ladyonorato">ladyonorato</a></td>
        		<td><a href="https://www.facebook.com/ladyonorato/">ladyonorato</a></td>
        		<td><br></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Salvatore</td>
        		<td>De Meo</td>
        		<td>salvatore.demeo@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/salv_de_meo">salv_de_meo</a></td>
        		<td><a href="https://www.facebook.com/DeMeoSalvatore/">DeMeoSalvatore</a></td>
        		<td><a href="https://www.instagram.com/salvatore_de_meo_/">salvatore_de_meo_</a></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Lara</td>
        		<td>Comi</td>
        		<td>lara.comi@europarl.europa.eu</td>
        		<td><br></td>
        		<td><br></td>
        		<td><br></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Angelo</td>
        		<td>Ciocca</td>
        		<td>angelo.ciocca@europarl.europa.eu</td>
        		<td><br></td>
        		<td><br></td>
        		<td><br></td>
        	</tr>
        	<tr>
        		<td>ITRE/INTA</td>
        		<td>Tiziana</td>
        		<td>Beghin</td>
        		<td>tiziana.beghin@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/beghin_t">beghin_t</a></td>
        		<td><a href="https://www.facebook.com/tizianabeghineuropa/">tizianabeghineuropa</a></td>
        		<td><br></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Isabella</td>
        		<td>Tovaglieri</td>
        		<td>isabella.tovaglieri@europarl.europa.eu</td>
        		<td><br></td>
        		<td><a href="https://www.facebook.com/IsabellaTovaglieriLega/">IsabellaTovaglieriLega</a></td>
        		<td><br></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Aldo</td>
        		<td>Patriciello</td>
        		<td>aldo.patriciello@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/PatricielloAldo">PatricielloAldo</a></td>
        		<td><a href="https://www.facebook.com/AldoPatriciello">AldoPatriciello</a></td>
        		<td><a href="https://www.instagram.com/aldopatriciello57/">aldopatriciello57</a></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Nicola</td>
        		<td>Danti</td>
        		<td>nicola.danti@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/DantiNicola">DantiNicola</a></td>
        		<td><a href="https://www.facebook.com/nicola.danti">nicola.danti</a></td>
        		<td><a href="https://www.instagram.com/nicoladantieu/">nicoladantieu</a></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Beatrice</td>
        		<td>Covassi</td>
        		<td>beatrice.covassi@europarl.europa.eu</td>
        		<td><br></td>
        		<td><br></td>
        		<td><br></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Ignazio</td>
        		<td>Corrao</td>
        		<td>ignazio.corrao@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/ignaziocorrao">ignaziocorrao</a></td>
        		<td><a href="https://www.facebook.com/Corraofb/">Corraofb</a></td>
        		<td><a href="https://www.instagram.com/ignaziocorrao/">ignaziocorrao</a></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Paolo</td>
        		<td>Borchia</td>
        		<td>paolo.borchia@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/paoloborchia">paoloborchia</a></td>
        		<td><a href="https://www.facebook.com/PaoloBorchiaofficial/">PaoloBorchiaofficial</a></td>
        		<td><a href="https://www.instagram.com/paoloborchia/">paoloborchia</a></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Matteo</td>
        		<td>Adinolfi</td>
        		<td>matteo.adinolfi@europarl.europa.eu</td>
        		<td><br></td>
        		<td><a href="https://www.facebook.com/MatteoAdinolfiLega">MatteoAdinolfiLega</a></td>
        		<td><br></td>
        	</tr>
        	<tr>
        		<td>ITRE</td>
        		<td>Patrizia</td>
        		<td>Toia</td>
        		<td>patrizia.toia@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/toiapatrizia">toiapatrizia</a></td>
        		<td><a href="https://www.facebook.com/ToiaPatrizia">ToiaPatrizia</a></td>
        		<td><a href="https://www.instagram.com/patriziatoia">patriziatoia</a></td>
        	</tr>
        	<tr>
        		<td>INTA</td>
        		<td>Massimiliano</td>
        		<td>Smeriglio</td>
        		<td>massimiliano.smeriglio@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/maxsmeriglio">maxsmeriglio</a></td>
        		<td><a href="https://www.facebook.com/massimiliano.smeriglio">massimiliano.smeriglio</a></td>
        		<td><a href="https://www.instagram.com/massimilianosmeriglio">massimilianosmeriglio</a></td>
        	</tr>
        	<tr>
        		<td>INTA</td>
        		<td>Dino</td>
        		<td>Giarrusso</td>
        		<td>dino.giarrusso@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/dinogiarrusso">dinogiarrusso</a></td>
        		<td><a href="https://www.facebook.com/ienadinogiarrusso">ienadinogiarrusso</a></td>
        		<td><br></td>
        	</tr>
        	<tr>
        		<td>INTA</td>
        		<td>Marco</td>
        		<td>Campomenosi</td>
        		<td>marco.campomenosi@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/mcampomenosi">mcampomenosi</a></td>
        		<td><a href="https://www.facebook.com/marco.campomenosi/">marco.campomenosi</a></td>
        		<td><br></td>
        	</tr>
        	<tr>
        		<td>INTA</td>
        		<td>Sergio</td>
        		<td>Berlato</td>
        		<td>sergio.berlato@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/SERGIOBERLATO">SERGIOBERLATO</a></td>
        		<td><a href="https://www.facebook.com/SergioBerlato.paginaufficiale/">SergioBerlato.paginaufficiale</a></td>
        		<td><a href="https://www.instagram.com/berlatosergioantonio_/">berlatosergioantonio_</a></td>
        	</tr>
        	<tr>
        		<td>INTA</td>
        		<td>Alessandra</td>
        		<td>Mussolini</td>
        		<td>alessandra.mussolini@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/Ale_Mussolini_">Ale_Mussolini_</a></td>
        		<td><br></td>
        		<td><br></td>
        	</tr>
        	<tr>
        		<td>INTA</td>
        		<td>Danilo Oscar</td>
        		<td>Lancini</td>
        		<td>danilooscar.lancini@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/doscarlancini">doscarlancini</a></td>
        		<td><a href="https://www.facebook.com/DOscarLancini/">DOscarLancini</a></td>
        		<td><a href="https://www.instagram.com/danilooscarlancini/">danilooscarlancini</a></td>
        	</tr>
        	<tr>
        		<td>INTA</td>
        		<td>Paolo</td>
        		<td>De Castro</td>
        		<td>paolo.decastro@europarl.europa.eu</td>
        		<td><a href="https://twitter.com/paolodecastro">paolodecastro</a></td>
        		<td><a href="https://www.facebook.com/paolo.decastro">paolo.decastro</a></td>
        		<td><br></td>
        	</tr>
        </tbody>
    </table>
</div>
