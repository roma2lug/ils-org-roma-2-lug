---
layout: post
title: Didattica Aperta 2013
created: 1383618223
---
Italian Linux Society e' lieta di supportare l'edizione 2013 di <a rel="nofollow" href="http://didatticaaperta.it/">Didattica Aperta</a>, annuale conferenza sul software libero a scuola che quest'anno si terra' sabato 16 novembre a Ivrea (TO).

La manifestazione di quest'anno presenta un <a rel="nofollow" href="http://didatticaaperta.it/programma-2013">ampio programma</a> con tutti i protagonisti della didattica libera e condivisa del nostro Paese: tra gli altri il <a rel="nofollow" href="http://wiildos.it/">progetto WiiLDOS</a> (che, oltre ad una sempre maggiormente popolare distribuzione GNU/Linux per la scuola, aggrega una delle piu' grandi comunita' di docenti freesoftware in Italia), i referenti di <a rel="nofollow" href="http://www.porteapertesulweb.it/">Porte Aperte sul Web</a> (grande opera di supporto e sviluppo di strumenti online, rigorosamente opensource, per le scuole), e gli interventi di Renzo Davoli e Norberto Patrignani, professori di alto profilo ben noti al pubblico anche oltre i confini dell'universo Linux.

Come sempre l'evento cerca di fare il punto sul rapporto tra tecnologia ed insegnamento, evidenziando come il software ed i contenuti liberi non siano solo strumenti educativi ma essi stessi stimolo alla partecipazione, alla collaborazione, allo scambio, alla creativita'. Un appuntamento importante per andare oltre una visione superficiale ed incompleta dell'innovazione della scuola, fatta di tablet e LIM da continuare ad usare per la fruizione passiva dei materiali, e per rimettere in discussione i fondamenti stessi della didattica, che dalla tecnologia dovrebbe essere potenziata anziche' vincolata.

Appuntamento dunque ad Ivrea, sabato 16 novembre. Invitati soprattutto gli insegnanti, e tutti coloro che desiderano capire (e costruire) una nuova scuola per i nostri giovani.
