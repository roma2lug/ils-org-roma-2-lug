---
layout: post
title: 'Report: Linux Day 2015'
created: 1458258901
---
<p style="text-align: center">
<img typeof="foaf:Image" src="/assets/images/linuxday_logo.png" alt="Linux Day">
</p>

<p>
Sabato 24 ottobre 2015 si è svolta la quindicesima edizione del <a href="https://www.linuxday.it/2015/">Linux Day</a>, la principale manifestazione italiana per la promozione di Linux e del software libero.
</p>

<p>
Come <a href="{% link _posts/2015-02-09-report-linux-day-2014.md %}">lo scorso anno</a> sono stati erogati due distinti questionari, uno rivolto al pubblico ed uno agli organizzatori, per avere una visione complessiva del massiccio evento distribuito; viene qui pubblicato il relativo <a href="/assets/files/RapportoLinuxDay2015.pdf">Report Linux Day 2015</a>, con dati, grafici, e considerazioni di massima.
</p>

<p>
Nonché un ennesimo invito al <a href="{% link _posts/2016-01-18-linux-presentation-day.md %}">Linux Presentation Day</a> che si svolgerà ad aprile, iniziativa molto simile a quella di ottobre ma di scala europea; auspichiamo che questo, oltre ad essere un nuovo appuntamento di riferimento per la comunità pro-linuxara, possa anche essere una occasione per rinnovare e sperimentare nuove forme divulgative.
</p>

<p>
Per restare aggiornati sui lavori in vista sia di aprile che di ottobre raccomandiamo, come sempre, di seguire l'<a rel="nofollow" href="https://twitter.com/linuxdayitalia">account Twitter</a> e la <a rel="nofollow" href="https://www.facebook.com/LinuxDayItalia">pagina Facebook</a> dedicati.
</p>
