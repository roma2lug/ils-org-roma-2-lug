---
layout: post
title: Protocollo con Informatici Senza Frontiere
image: /assets/posts/images/isf_ils.png
---

L'assegnazione del <a href="{% link _posts/2020-10-08-premio-jacopo-crivelli-per-il-linux-day-2020.md %}">Premio Jacopo Crivelli</a> in sede di Linux Day Online 2020, assegnato al [talk di Alexander Palummo](https://video.linux.it/videos/watch/afdd7d5e-f578-4d89-a883-eac7401fcb44) e destinato all'associazione Informatici Senza Frontiere per progetti legati al recupero di hardware, è stata l'occasione per un confronto tra ILS e appunto [ISF](https://www.informaticisenzafrontiere.org/), culminato con la sottoscrizione di un protocollo di intesa tra le due realtà.

<!--more-->

Già nel recente passato ILS e ISF si sono incontrate, ad esempio sul progetto [OpenDidattica.org](https://opendidattica.org/), e più volte le due hanno manifestato interesse ed attività in aree tra di loro affini. Auspichiamo che la formalizzazione di questo rapporto possa stimolare un maggiore coinvolgimento dei rispettivi gruppi e nuove collaborazioni, soprattutto in tema di recupero consapevole di hardware e di tecnologie assistive (libere, aperte e accessibili a chiunque).
