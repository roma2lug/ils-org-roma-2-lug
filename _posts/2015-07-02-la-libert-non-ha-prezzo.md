---
layout: post
title: La Libertà non ha Prezzo
created: 1435789740
---
Da sempre la community di LUG e simpatizzanti opera, tutti i giorni ed in tutte le città, per promuovere e far conoscere il software libero, per farlo utilizzare ed adottare, e per stimolare la consapevolezza digitale di tutti.

Ma, spesso, la semplice promozione non basta. Il software libero è libero, ma è anche software, ed in quanto tale necessita di risorse per essere sviluppato e soprattutto per essere mantenuto nel tempo. Tantopiù i progetti più piccoli e meno conosciuti al grande pubblico, meno sostenibili, ma utilissimi ed anzi indispensabili in contesti sensibili come la scuola.

Per questo motivo <a href="/">Italian Linux Society</a> annuncia oggi una nuova iniziativa: <a href="http://donazioni.linux.it/">donazioni.linux.it</a>. Il sito è destinato ad ospitare raccolte di fondi, a cadenza più o meno periodica, condotte direttamente dall'associazione stessa o comunque mirate al sostegno dello sviluppo di codice libero e aperto.

La piattaforma viene inaugurata con una campagna di finanziamento a favore di Lightboard, che pienamente rappresenta lo spirito di solidarietà dell'iniziativa: progetto piccolo e probabilmente poco noto, sinora condotto a titolo volontario dal suo manutentore Giovanni Incammicia, ma dedicato a facilitare l'utilizzo e la diffusione della <a rel="nofollow" href="https://wiildos.wikispaces.com/Lavagna+col+wiimote">"Wiimote Lavagna Digitale"</a> nelle scuole, in particolare quelle meno dotate di risorse tecnologiche. Una volta completata, l'applicazione renderà più agevole realizzare una lavagna interattiva a bassissimo costo impiegando un controller da poche decine di euro, un RaspberryPI ed un semplice proiettore.

Ma, naturalmente, non ci limitiamo ad annunciare la raccolta e chiedere passivamente il vostro contributo: i soci di Italian Linux Society mettono a disposizione i primi 3000 euro da destinare al progetto per garantire la prima fase di sviluppo, quella essenziale per consolidare la base di codice esistente e renderla facilmente fruibile. Arrivando, con le vostre donazioni, a quota 4500 euro, renderemo possibile l'implementazione di funzioni più complesse ed utili in classe per aiutare ulteriormente i nostri maestri e docenti.

Per incentivare e stimolare le donazioni, i soldi delle quote di coloro che si iscriveranno nel mese di luglio ad Italian Linux Society saranno direttamente destinati a questa campagna: con 25 euro si potrà dunque dare il proprio contributo tangibile al software libero, e beneficiare di <a href="/iscrizione">tutti i servizi</a> rivolti ai nostri soci.

Sostieni anche tu il software libero con una donazione, anche da pochi euro. <a href="http://donazioni.linux.it/">Con l'aiuto di tutti, facciamo il software di tutti</a>.
