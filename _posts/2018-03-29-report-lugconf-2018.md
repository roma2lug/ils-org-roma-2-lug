---
layout: post
title: Report LUGConf 2018
created: 1522320804
---
<p>
Nel pomeriggio di sabato 24 marzo, nel contesto di <a rel="nofollow" href="https://merge-it.net/">MERGE-it</a> e a seguito dell'assemblea ILS, si è tenuto <a href="{% link _posts/2018-01-30-lugconf-2018.md %}">un incontro pubblico e aperto di confronto coi LUG</a>. Due in particolare sono stati i macro-temi trattati: il futuro del <a href="https://www.linuxday.it/">Linux Day</a> e l'introduzione delle "sezioni locali" di ILS.
</p>

<h3>Linux Day</h3>

<p>
Si è voluto porre all'attenzione dell'assemblea il costante declino nel numero di eventi registrati ogni anno, da diversi anni a questa parte, e l'esigenza di analizzare la situazione.
</p>
<p>
Quattro le proposte iniziali sottoposte:
</p>
<ul>
    <li>continuare a svolgere il Linux Day così come è</li>
    <li>sopprimerlo e confluire nel <a rel="nofollow" href="https://www.softwarefreedomday.org/">Software Freedom Day</a></li>
    <li>organizzarlo non più su base autonoma locale ma su base strutturata regionale</li>
    <li>eliminarlo in toto</li>
</ul>
<p>
Nel corso della discussione, altre alternative sono state prese in esame:
</p>
<ul>
    <li>eliminarlo, e sostituirlo con manifestazioni nazionali di taglio tematico</li>
    <li>organizzarlo su base provinciale</li>
</ul>
<p>
Rapidamente è emersa una diffusa volontà di conservare il modello attuale, imputando il declino del Linux Day ad una generalizzata crisi dei LUG (discussa ed approfondita nella fase successiva della riunione).
</p>
<p>
Si conclude che il Linux Day 2018 si svolgerà secondo le consuete modalità, delegando maggiori incarichi alla community. A tal proposito, è stato predisposto un repository GitLab che permetta a tutti di tenere
traccia delle cose da fare, entro quando sono da fare, e chi le fa: <a rel="nofollow" href="https://gitlab.com/ItalianLinuxSociety/LinuxDay">https://gitlab.com/ItalianLinuxSociety/LinuxDay</a>
</p>
<p>
Ulteriori discussioni in tal senso sono rimandate alla mailing list pubblica dedicata all'evento: <a href="https://lists.linux.it/listinfo/linuxday-idee">https://lists.linux.it/listinfo/linuxday-idee</a>
</p>

<h3>Sezioni Locali</h3>

<p>
Riprendendo quanto emerso sulle difficoltà - soprattutto amministrative e burocratiche - dei LUG, nonché quanto discusso nel corso della mattinata in sede di assemblea ILS, è stato presentato il proposito di ILS di formalizzare il proprio supporto amministrativo ai LUG introducendo la nozione di "sezione locale" dell'associazione nazionale, in modo da centralizzare gli oneri ed ottimizzare le risorse.
</p>
<p>
La proposta è stata accolta con curiosità ed interesse, benché siano ancora da definire i vincoli per l'adesione ed i limiti ragionevolmente sostenibili di tale supporto.
</p>
<p>
A settembre, quando saranno infine confermate le nuove regole introdotte dalla <a rel="nofollow" href="https://italianonprofit.it/riforma/">Riforma del Terzo Settore</a>, la discussione sui contenuti del nuovo statuto ILS sarà estesa a tutti i potenziali interessati per soddisfare le esigenze dei più. Vengono comunque anticipate le necessità di:
</p>
<ul>
    <li>inquadrare ILS in una forma che assecondi bisogni diversi (organizzazione di volontariato, associazione di promozione sociale, o altro)</li>
    <li>provvedere ad una assicurazione per tutti i membri dell'associazione, requisito spesso indispensabile per accedere alle risorse messe a disposizione dai comuni</li>
    <li>prevedere delle "sedi operative" locali oltre alla sede legale unica di ILS, altro requisito utile per ottenere spazi e risorse dai singoli comuni dove risiedono le sezioni locali</li>
</ul>
