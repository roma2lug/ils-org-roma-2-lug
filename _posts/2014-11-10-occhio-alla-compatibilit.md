---
layout: post
title: Occhio alla Compatibilità!
created: 1415643726
---
Quando si acquista un nuovo computer con l'intenzione di usare Linux, oltre a eventualmente prepararsi a <a href="https://www.sistemainoperativo.it/">chiedere il rimborso delle licenze</a> di software pre-installato ed indesiderato è comunque consigliato documentarsi preventivamente sulla compatibilità dell'hardware. A tutt'oggi, infatti, purtroppo non tutti i componenti e le periferiche godono di pieno supporto tramite drivers completamente liberi e opensource, per via delle resistenze dei produttori a rilasciare codice aperto (o anche solo documentazione dettagliata) per il funzionamento dei loro dispositivi.

Con l'obiettivo di facilitare questa fase è stata pubblicata una nuova sezione su <a href="https://www.linuxsi.com/">LinuxSi</a>, storico e popolare sito (da qualche tempo <a href="{% link _posts/2013-07-02-linux-preinstallato-come-e-perch.md %}">curato direttamente da Italian Linux Society</a>) dedicato appunto a chi vuole un nuovo PC con Linux e cerca riferimenti e contatti: nella pagina <a href="https://www.linuxsi.com/produttori/">"Produttori"</a> è da oggi disponibile una panoramica dei più comuni e noti marchi di personal computer e notebook, classificati secondo la compatibilità dei rispettivi prodotti con Linux e disposti su una scala di immediata lettura.

Tale misura di compatibilità viene valutata su dati statistici, a partire dalle recensioni raccolte sull'utilissimo <a rel="nofollow" href="https://www.h-node.org/">"h-node"</a> (cui vi invitiamo a partecipare!), e benché certo non rappresenti un parametro preciso ed infallibile permette comunque di avere una idea dei migliori (o peggiori) produttori presso cui rivolgersi per l'acquisto di un computer che sia quanto più possibile utilizzabile fin da subito ed al pieno delle potenzialità con Linux.

Speriamo che questo nuovo strumento di analisi e comparazione possa aiutare qualcuno ad avere meno grattacapi con il suo prossimo nuovo computer. Acquistate consapevolmente!
